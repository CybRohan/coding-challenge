﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVCWebApplication.Interfaces;
using MVCWebApplication.Models;

namespace MVCWebApplication.Controllers
{
    public class NewsController : Controller
    {
        private readonly ILogger<NewsController> _logger;
        private readonly INews _news;

        public NewsController(ILogger<NewsController> logger, INews news)
        {
            _logger = logger;
            _news = news;
        }

        public IActionResult Index()
        {
            var model = _news.GetAllNews();
            return View(new NewsModel { AllNews = model });
        }

        public IActionResult Details(int id)
        {
            var model = _news.GetNewsById(id);
            return View("Details", new NewsModel { News = model });
        }
    }
}