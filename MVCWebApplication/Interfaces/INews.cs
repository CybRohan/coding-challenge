﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MVCWebApplication.Models;

namespace MVCWebApplication.Interfaces
{
    public interface INews
    {
        News GetNewsById(int id);
        List<News> GetAllNews();
    }
}
