﻿using System.Collections.Generic;

namespace MVCWebApplication.Models
{
    public class NewsModel
    {
        public News News { get; set; }
        public List<News> AllNews { get; set; }
    }

    public class News
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ReadTime { get; set; }
        public string Image { get; set; }
    }
}