﻿using System;
using System.Collections.Generic;
using System.Linq;
using MVCWebApplication.Interfaces;
using MVCWebApplication.Models;

namespace MVCWebApplication.Services
{
    public class NewsService : INews
    {
        static readonly List<News> newsData = new List<News>
            {
                new News
                    {
                        Id = 1,
                        Title = "Why only few get success in achieving their goal?",
                        Description =
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
                        ReadTime = "4 minute read",
                        Image = "/images/image1.jpg"
                    },
                new News
                    {
                        Id = 2,
                        Title = "Happiness is that difficult to achieve?",
                        Description =
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
                        ReadTime = "6 minute read",
                        Image = "/images/image2.jpg"
                    },
                new News
                    {
                        Id = 3,
                        Title = "A room without books is like a body without a soul.",
                        Description =
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
                        ReadTime = "8 minute read",
                        Image = "/images/image3.jpg"
                    },
                new News
                    {
                        Id = 4,
                        Title = "Be the change that you wish to see in the world.",
                        Description =
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
                        ReadTime = "7 minute read",
                        Image = "/images/image4.jpg"
                    }
            };

        public News GetNewsById(int id) => newsData.FirstOrDefault(x => x.Id == id);

        public List<News> GetAllNews()
        {
            var rand = new Random();
            return newsData.OrderBy(x => rand.Next()).ToList();
        }
    }
}