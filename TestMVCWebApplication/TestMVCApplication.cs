using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MVCWebApplication.Interfaces;
using MVCWebApplication.Models;

namespace TestMVCWebApplication
{
    [TestClass]
    public class TestMVCApplication
    {
        [TestMethod]
        public void GetNewsById_Given_Valid_Id_Should_Return_NewsDetails()
        {
            //Arrange
            var newsId = 11;
            var newsMock = new Mock<INews>();
            var newsList = GetAllNewsTestData();
            var newsItem = newsList.FirstOrDefault(x => x.Id == newsId);
            newsMock.Setup(p => p.GetNewsById(It.IsAny<int>())).Returns(newsItem);

            //Act
            var result = newsMock.Object.GetNewsById(newsId);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(News));
            Assert.AreEqual(result.Id, newsId);
        }

        [TestMethod]
        public void GetNewsById_Given_Invalid_Id_Should_Return_Null()
        {
            //Arrange
            var newsId = -1;
            var newsMock = new Mock<INews>();
            newsMock.Setup(p => p.GetNewsById(newsId)).Returns((News)null);

            //Act
            var result = newsMock.Object.GetNewsById(newsId);

            //Assert
            Assert.IsNull(result);
            Assert.IsTrue(0 > newsId);
        }

        [TestMethod]
        public void GetAllNews_Should_Return_More_Than_One_Item()
        {
            //Arrange
            var newsMock = new Mock<INews>();
            var newsList = GetAllNewsTestData();

            newsMock.Setup(p => p.GetAllNews()).Returns(newsList);

            //Act
            var result = newsMock.Object.GetAllNews();

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<News>));
            Assert.IsTrue(result.Count > 1);
        }

        [TestMethod]
        public void GetAllNews_News_Title_Must_Be_More_Than_Fifteen_Characters()
        {
            //Arrange
            var newsMock = new Mock<INews>();
            var newsList = GetAllNewsTestData();

            newsMock.Setup(p => p.GetAllNews()).Returns(newsList);

            //Act
            var result = newsMock.Object.GetAllNews();

            //Assert
            Assert.IsNotNull(result);
            foreach (var news in result)
                Assert.IsTrue(news.Title.Length > 15);
        }

        [TestMethod]
        public void GetAllNews_News_Image_Path_Must_Not_Be_Null()
        {
            //Arrange
            var newsMock = new Mock<INews>();
            var newsList = GetAllNewsTestData();

            newsMock.Setup(p => p.GetAllNews()).Returns(newsList);

            //Act
            var result = newsMock.Object.GetAllNews();

            //Assert
            Assert.IsNotNull(result);
            foreach (var news in result)
                Assert.IsNotNull(news.Image);
        }

        private List<News> GetAllNewsTestData() =>
            new List<News>
                {
                    new News
                    {
                        Id = 11,
                        Title = "Why only few get success in achieving their goal?",
                        Description =
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
                        ReadTime = "4 minute read",
                        Image = "/images/image1.jpg"
                    },
                new News
                    {
                        Id = 12,
                        Title = "Happiness is that difficult to achieve?",
                        Description =
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
                        ReadTime = "6 minute read",
                        Image = "/images/image2.jpg"
                    },
                new News
                    {
                        Id = 13,
                        Title = "A room without books is like a body without a soul.",
                        Description =
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
                        ReadTime = "8 minute read",
                        Image = "/images/image3.jpg"
                    },
                new News
                    {
                        Id = 14,
                        Title = "Be the change that you wish to see in the world.",
                        Description =
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
                        ReadTime = "7 minute read",
                        Image = "/images/image4.jpg"
                    }
                };

    }
}